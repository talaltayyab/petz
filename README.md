# README #

Sample project to demo calling an external API and returning results.

## Building

In the root directory run

    dotnet restore
    dotnet build

## Running the web app

In the root directory run

    dotnet run dotnet run --project petz.webapp

Launch a browser at http://localhost:5000

## Running the tests

In the root directory run

    dotnet test
