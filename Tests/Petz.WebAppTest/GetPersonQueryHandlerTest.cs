using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Petz.WebApp.Queries;
using Petz.WebApp.Service;
using Petz.WebApp.Service.Model;

namespace Petz.WebApp.Test
{
    [TestClass]
    public class GetPersonQueryHandlerTest
    {
        private Mock<IAglApiService>  _mockAglApiService = new Mock<IAglApiService>();
        private Mock<ILogger<GetPersonAndPetsQueryHandler>> _loggingService = new Mock<ILogger<GetPersonAndPetsQueryHandler>>();

        [TestMethod]
        public async Task GivenPersonsWithPetsWhenHandlerCalledThenReturnsOnlyPersonsWithCatsOrderedByName()
        {
            // Arrange 
            var persons =
                new PersonsBuilder()
                    .AddNewPerson().WithName("Person 1").WithGender("Male")
                        .AddNewPet().WithName("Dog 11").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Fish 11").WithType("Fish").BuildPet()
                        .AddNewPet().WithName("Cat 12").WithType("Cat").BuildPet()
                        .AddNewPet().WithName("Cat 11").WithType("Cat").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 2").WithGender("Male")
                        .AddNewPet().WithName("Dog 21").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 22").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 3").WithGender("Female")
                        .AddNewPet().WithName("Cat 31").WithType("Cat").BuildPet()
                        .AddNewPet().WithName("Dog 31").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 32").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 4").WithGender("Female")
                        .AddNewPet().WithName("Dog 41").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 42").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .BuildPersons();

            SetupMocks(persons);

            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(2);

            var result1 = result[0];
            result1.Gender.Should().Be("Male");
            result1.Pets.Length.Should().Be(2);
            result1.Pets[0].Name.Should().Be("Cat 11");
            result1.Pets[1].Name.Should().Be("Cat 12");

            var result2 = result[1];
            result2.Gender.Should().Be("Female");
            result2.Pets.Length.Should().Be(1);
            result2.Pets[0].Name.Should().Be("Cat 31");
        }

        [TestMethod]
        public async Task GivenEmptyDataWhenHandlerCalledThenReturnsEmptyResult()
        {
            // Arrange 
            var persons = new PersonsBuilder().BuildPersons();

            SetupMocks(persons); 

            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public async Task GivenPersonsWithOnlyDogsWhenHandlerCalledThenReturnsEmptyResult()
        {
            // Arrange 
            var persons =
                new PersonsBuilder()
                    .AddNewPerson().WithName("Person 1").WithGender("Male")
                        .AddNewPet().WithName("Dog 11").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 12").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 2").WithGender("Male")
                        .AddNewPet().WithName("Dog 21").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 22").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 3").WithGender("Female")
                        .AddNewPet().WithName("Dog 31").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 32").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 4").WithGender("Female")
                        .AddNewPet().WithName("Dog 41").WithType("Dog").BuildPet()
                        .AddNewPet().WithName("Dog 42").WithType("Dog").BuildPet()
                        .BuildPerson()
                    .BuildPersons();

            SetupMocks(persons);

            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public async Task GivenPersonsWithNoPetsWhenHandlerCalledThenReturnsEmptyResult()
        {
            // Arrange 
            var persons =
                new PersonsBuilder()
                    .AddNewPerson().WithName("Person 1").WithGender("Male")
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 2").WithGender("Male")
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 3").WithGender("Female")
                        .BuildPerson()
                    .AddNewPerson().WithName("Person 4").WithGender("Female")
                        .BuildPerson()
                    .BuildPersons();

            SetupMocks(persons);

            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public async Task GivenPersonsWithNullGenderWhenHandlerCalledThenIgnoresPerson()
        {
            // Arrange 
            SetupMocks(null);
          
            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public async Task GivenPersonWithNoGenderThenReturnsReturnsEmptyResult()
        {
            // Arrange 
            var persons =
                 new PersonsBuilder()
                     .AddNewPerson().WithName("Person 1").WithGender("Female")
                         .AddNewPet().WithName("Cat 11").WithType("Cat").BuildPet()
                         .BuildPerson()
                     .AddNewPerson().WithName("Person 2").WithGender(null)
                         .AddNewPet().WithName("Dog 21").WithType("Cat").BuildPet()
                         .AddNewPet().WithName("Dog 22").WithType("Fish").BuildPet()
                         .BuildPerson()
                     .BuildPersons();

            SetupMocks(persons);

            // Act
            var handler = new GetPersonAndPetsQueryHandler(_mockAglApiService.Object, _loggingService.Object);
            var result = await handler.Handle(new GetPersonAndPetsQuery(), new System.Threading.CancellationToken());

            // Assert
            result.Count.Should().Be(1);

            var result1 = result[0];
            result1.Gender.Should().Be("Female");
            result1.Pets.Length.Should().Be(1);
            result1.Pets[0].Name.Should().Be("Cat 11");

            _loggingService.Verify(x => x.Log(
                LogLevel.Warning, 
                It.IsAny<EventId>(), 
                It.IsAny<It.IsAnyType>(), 
                It.IsAny<Exception>(), 
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()),
                Times.Once);
        }

        private void SetupMocks(List<Person> persons)
        {
            _mockAglApiService.Setup(ps => ps.GetPersonsAndPets()).Returns(Task.FromResult(persons));

            _loggingService = new Mock<ILogger<GetPersonAndPetsQueryHandler>>();
            _loggingService.Setup(l => l.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<GetPersonAndPetsQueryHandler>(), It.IsAny<Exception>(), It.IsAny<Func<GetPersonAndPetsQueryHandler, Exception, string>>()));

        }
    }
}
