﻿using System.Collections.Generic;
using Petz.WebApp.Service.Model;

namespace Petz.WebApp.Test
{
    public class PersonBuilder
    {
        private readonly PersonsBuilder _parent;
        private readonly Person _person;
        private readonly List<Pet> _pets;

        public PersonBuilder(PersonsBuilder parent)
        {
            _parent = parent;
            _person = new Person();
            _pets= new List<Pet>();
        }

        public PersonBuilder WithName(string name)
        {
            _person.Name = name;
            return this;
        }

        public PersonBuilder WithGender(string gender)
        {
            _person.Gender = gender;
            return this;
        }

        public PetBuilder AddNewPet()
        {
            return new PetBuilder(this);
        }

        public void AddPet(Pet pet)
        {
            _pets.Add(pet);
        }

        public PersonsBuilder BuildPerson()
        {
            _person.Pets = _pets.Count > 0 ? _pets.ToArray() : null;
            _parent.AddPerson(_person);
            return _parent;
        }
    }
}
