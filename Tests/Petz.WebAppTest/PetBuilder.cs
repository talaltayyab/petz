﻿using Petz.WebApp.Service.Model;

namespace Petz.WebApp.Test
{
    public class PetBuilder
    {
        private readonly PersonBuilder _personBuilder;
        private readonly Pet _pet;

        public PetBuilder(PersonBuilder _personBuilder)
        {
            this._personBuilder = _personBuilder;
            _pet = new Pet();
        }
        public PetBuilder WithName(string name)
        {
            _pet.Name = name;
            return this;
        }

        public PetBuilder WithType(string type)
        {
            _pet.Type = type;
            return this;
        }

        public PersonBuilder BuildPet()
        {
            _personBuilder.AddPet(_pet);
            return _personBuilder;
        }
    }
}