﻿using System.Collections.Generic;
using System.Linq;
using Petz.WebApp.Service.Model;

namespace Petz.WebApp.Test
{
    public class PersonsBuilder
    {
        List<Person> _persons = new List<Person>();

        public PersonBuilder AddNewPerson()
        {
            return new PersonBuilder(this);
        }

        public void AddPerson(Person person)
        {
            _persons.Add(person);
        }

        public List<Person> BuildPersons()
        {
            return _persons.ToList();
        }
    }
}