﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Petz.WebApp.Queries;
using Petz.WebApp.ViewModel;

namespace Petz.WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IMediator _mediator;
        public bool IsError { get; private set; } = false;

        public IEnumerable<PersonViewModel> Persons { get; private set; }
        public IndexModel(ILogger<IndexModel> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public async Task OnGet()
        {
            try
            {
                Persons = await _mediator.Send(new GetPersonAndPetsQuery());
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, "An error occurred while getting person and pets");
                IsError = true;
            }
            
        }
    }
}
