﻿namespace Petz.WebApp.ViewModel
{
    public class PersonViewModel
    {
        public string Gender { get; set; }
        
        public PetViewModel[] Pets { get; set; }
    }
}
