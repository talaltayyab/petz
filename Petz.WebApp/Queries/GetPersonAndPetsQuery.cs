﻿using System.Collections.Generic;
using MediatR;
using Petz.WebApp.ViewModel;

namespace Petz.WebApp.Queries
{
    public class GetPersonAndPetsQuery : IRequest<List<PersonViewModel>>
    {
    }
}
