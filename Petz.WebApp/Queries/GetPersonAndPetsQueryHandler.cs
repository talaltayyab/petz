﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Petz.WebApp.Service;
using Petz.WebApp.ViewModel;

namespace Petz.WebApp.Queries
{
    public class GetPersonAndPetsQueryHandler : IRequestHandler<GetPersonAndPetsQuery, List<PersonViewModel>>
    {
        private readonly ILogger<GetPersonAndPetsQueryHandler> _logger;
        private readonly IAglApiService _aglApiService;
        private readonly string[] _genders = { "Male", "Female" };


        public GetPersonAndPetsQueryHandler(IAglApiService aglApiService, ILogger<GetPersonAndPetsQueryHandler> logger)
        {
            _aglApiService = aglApiService;
            _logger = logger;
        }
        public async Task<List<PersonViewModel>> Handle(GetPersonAndPetsQuery request, CancellationToken cancellationToken)
        {
            var persons = await _aglApiService.GetPersonsAndPets();

            var vm = new List<PersonViewModel>();

            if (persons == null)
            {
                return vm;
            }

            foreach (var person in persons.Where(p => p.Pets?.Any(pet => pet.Type == "Cat") == true))
            {
                if (_genders.All(x => x != person.Gender))
                {
                    _logger.LogWarning("Found unknown gender {gender}, skipping entry of record {@record}", person.Gender, person);
                    continue;
                }

                vm.Add(new PersonViewModel()
                {
                    Gender = person.Gender,
                    Pets = person.Pets.Where(pets => pets.Type == "Cat").OrderBy(pets => pets.Name).Select(p => new PetViewModel()
                    {
                        Name = p.Name
                    }).ToArray()
                });
            }

            return vm;
        }
    }
}