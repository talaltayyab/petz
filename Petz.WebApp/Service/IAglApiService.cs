﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Petz.WebApp.Service.Model;

namespace Petz.WebApp.Service
{
    public interface IAglApiService
    {
        Task<List<Person>> GetPersonsAndPets();
    }
}
